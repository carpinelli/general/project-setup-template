NAME
    The name of the command or function, followed by a one-line description of what it does.
SYNOPSIS
    In the case of a command, you get a formal description of how to run it and what command line options it takes.
DESCRIPTION
    A textual description of the functioning of the command or function.
EXAMPLES
    Some examples of common usage.
SEE ALSO
    A list of related commands or functions.
BUGS
    List known bugs.
AUTHOR
   Specify your contact information.
COPYRIGHT
    Specify your copyright information.
EXIT STATUS
    ???
ENVIRONMENT
    ???
FILES
    ???
HISTORY
    ???

|Section|Description|
|---|---|
|1|Executable shell commands|
|2|System calls (functions provided by the kernel)|
|3|Library calls (functions within program libraries)|
|4|Special files (usually found in /dev)|
|5|File formats and conventions eg /etc/passwd|
|6|Games|
|7|Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)|
|8|System administration commands (usually only for root)|
|9|Kernel routines [Non standard]|
