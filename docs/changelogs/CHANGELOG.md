# <PROJECT> v0.0.00

<DESCRIPTION.>

---

<!--
## Options (Stolen from GitLab):
    added: New feature
    fixed: Bug fix
    changed: Feature change
    deprecated: New deprecation
    removed: Feature removal
    security: Security fix
    performance: Performance improvement
    other: Other

---

## Current Version

### <RELEASE_VERSION> [DATE] Changelog:
* <LATEST_CHANGE>
* <SECOND_LATEST_CHANGE>

#### Features
* <HIGHLIGHTED_FEATURE>

#### Bug Fixes
* <HIGHLIGHTED_BUG_FIX>

---


### Previous Versions

#### <RELEASE_VERSION> [DATE] Changelog:
* <LATEST_CHANGE>
* <SECOND_LATEST_CHANGE>

##### Features
* <HIGHLIGHTED_FEATURE>

##### Bug Fixes
* <HIGHLIGHTED_BUG_FIX>

---
-->


## Git Generated Changelog
See [GITCHANGELOG.md](GITCHANGELOG.md)

---


## License

See [README](README.md) and [LICENSE](LICENSE).

---

