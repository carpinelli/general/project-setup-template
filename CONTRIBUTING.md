# Style Guidelines

## Code Blocks
In languages that use non-whitespace characters to denote code blocks,
the opening and closing character should be on on a newline, by t.

---


## JavaScript/TypeScript

### Functions
Function statements only.

### Statements
All statements should end with a semicolon, where possible.

---


## C/C++

### Using Namespace
```using namespace``` statements are never allowed.

---


## Python

### Be Pythonic
Be Pythonic, where possible. To new Python devs, this essentially means
leveraging the unique and powererful tools that are available in Python.
This also usually has an added benefit of tersing the code quite a bit.

### PEP8
Try to follow PEP8 guidelines as much as possible, but be reasonable and
don't be afraid to ignore the guidelines if it makes sense.

---


## File Names
All file names except Python must use camelcase. Python files must be
snakecase.

## Directory Names
each-word-of-dirname

---

## Markdown formatting

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#markdown-formatting)

- Files must be encoded in UTF-8 and do not have executable permission.
- Page headers should be indented with hashes (`#`) rather than equal signs (`=`).
- Bullet lists should be indented with hyphens (`-`).
- Characters that are special for Markdown must be escaped. Example: `\*`.
- Use `inline code formatting` for file names unless they are links.
- All code blocks should be enclosed in backticks, with language specified.
- Use spaces for indentation to keep documents readable in plain text format.


## Script formatting

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#script-formatting)

- File must be encoded in UTF-8.
- Package scripts `build.sh` and `*.subpackage.sh` should not have executable permission.
- Use tabs for indentation in Shell scripts.
- Use spaces for indentation in Python scripts.
- Parenthesis of functions should not be preceded with a space.
- Avoid trailing spaces and tabs.
- Lines shouldn't be longer than 80 characters.
- Comments should be reasonable, compact and attached to a place they refer to. Do not indent them if not necessary.
- Banner comments in `build.sh` or `*.subpackage.sh` are not allowed.


## Shell script coding practices

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#shell-script-coding-practices)

### General

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#general)

- Dollar parentheses `$()` rather than backticks ` `` ` should be employed in command substitution.

### Build script (`build.sh`)

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#build-script-buildsh)

- Usage of `sudo` or `su` in build scripts is disallowed.
- Global scope variables must be defined in `termux_step_pre_configure()` function unless they are part of package metadata.
- Local (step-specific) variables must be defined with keyword `local`. If variable does not have such keyword, it automatically becomes visible outside of function where it was defined.
- Do not export variables if not necessary.
- Utility `install` is preferred over `cp` as the file installation program.
- Do not hardcode version numbers. Instead, use the `$TERMUX_PKG_VERSION` and `$TERMUX_PKG_REVISION` variables.
- Do not hardcode Termux prefix directory. Instead, use the `$TERMUX_PREFIX` variable.
- Do not hardcode Termux home directory. Instead, use the `$TERMUX_ANDROID_HOME` variable.


## Patches

[](https://github.com/termux/termux-packages/wiki/Coding-guideline#patches)

- Files must be in format usable by GNU `patch` utility and do not have executable permission.
- Do not hardcode Termux prefix directory. Use `@TERMUX_PREFIX@` instead.
- Do not hardcode Termux home directory. Use `@TERMUX_HOME@` instead.
- Highly recommended to document changes unless they are just FHS path replacements.


---


## More Style Guidelines
These are not the only style guidelines. More will be added,
and are in active effect.

### To Add:
* Directory structure
* Filenames
* Variable names
* Line spacing
* More/etc.

---


## License

See [README](README.md) and [LICENSE](LICENSE).

---
