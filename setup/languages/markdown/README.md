# <PROJECT>

<DESCRIPTION.>

---


<!--
## Deployment Link
<[DEPLOYMENT_LINK]()>

---


## Technologies
- Lorem ipsum.
- Lorem ipsum.

---


---


## Intended Updates
- Lorem ipsum.
- Lorem ipsum.


## Ideas
- ~~Lorem ipsum.~~
- ~~Lorem ipsum.~~
- Lorem ipsum.

---


## Unresolved Issues
- Lorem ipsum.
- Lorem ipsum.

---


## Building

### React/Vite
	cd <PROJECTS/BUILDS_DIRECTORY>
	git clone <GIT_URL> <DIR_NAME>/
	cd <DIR_NAME>/
	npm install
	npm run build

## Usage
Lorem ipsum
```Lorem ipsum.```
    Lorem ipsum.

---

-->

## Contact
Joseph Carpinelli <carpinelli.dev@protonmail.ch>.

---


## Contributing
This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the licenses, styles, and goals of
the project. Please see the [CONTRIBUTING.md](CONTRIBUTING.md) file, if
available. Forking or copying is encouraged, just ensure to uphold the
licenses terms.

---


## License
The software and related files in this project are licensed under the
AGPL 3.0 license or higher. All non-software related files are licensed
under the Creative Commons Attribution Share Alike 4.0 International
license. Unless any libraries used legally require otherwise, all
contributions are made under the
[GNU Affero General Public License v3 or higher](https://www.gnu.org/licenses/agpl-3.0.html).
See the [LICENSE](LICENSE).

In cases where a library is used that requires a different or additional
license, and that license is not included, kindly inform the email
provided in the "Contact" section. Corrections will be made ASAP.

You may be released from the requirements of the included license by
purchasing a commercial license. Buying such a license is mandatory
if you want to modify or otherwise use the software for commercial
activities involving the software without disclosing the source code
of your own applications. Please note this may not always be possible
due to the licensing requirements of included projects as well as
other determining factors.

To purchase a commercial license, where available, send an
email to <carpinelli.dev@protonmail.ch>.

---

