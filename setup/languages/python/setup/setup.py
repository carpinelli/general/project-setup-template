#! python3
#! /usr/bin/env python3 -i
# -*- coding: utf-8 -*-


import sys
import os
from shutil import rmtree

from setuptools import find_packages, setup, Command


""" Meta-Data """
NAME="med-vs"
VERSION="0.0.1a1"
DESCRIPTION=(
    "Format and validate tables containing data ultimately intended for METRC."
    )
LONG_DESCRIPTION=()
LONG_DESCRIPTION_CONTENT_TYPE="text/markdown"
URL="https://github.com/jcarpinelli/med-vs"
AUTHOR="Joseph Carpinelli"
AUTHOR_EMAIL="jcarpinelli@acm.org"
LICENSE="GPLv3"
CLASSIFIERS=[
        # Trove classifiers:
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Customer Service",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Database"
]
KEYWORDS="csv excel med metrc plants tracker"
PROJECT_URLS={
# Project URL Examples
#     'Documentation': 'https://',
#     'Funding': 'https://',
    'Say Thanks!': 'http://bitbucket.org/openpyxl/openpyxl/',
#     'Source': 'https://',
#     'Tracker': 'https://',
}
REQUIRED=[
#    "xlrd",
    "openpyxl>=2.6.2"
]
REQUIRES_PYTHON=">=3.6.0"
""" End Meta-Data """


""" LICENSE, README, and VERSION """
with open("LICENSE", encoding="utf-8") as f:
    license = f.read()

Working_Directory = os.path.abspath(os.path.dirname(__file__))

# Import README to use as the long_description (if in MANIFEST.in)
try:
    with open(os.path.join(Working_Directory, "README.md"),
                 encoding="utf-8") as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(Working_Directory, project_slug, "__version__.py")
              ) as f:
        exec(f.read(), about)
else:
    about["__version__"] = VERSION
""" End LICENSE, README, and VERSION """


setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type=LONG_DESCRIPTION_CONTENT_TYPE,
    url=URL,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    keywords=KEYWORDS,
    project_urls=PROJECT_URLS,
    packages=find_packages(exclude=["contrib", "docs", "tests*"]),
    # If your project contains any single-file Python modules that aren’t
    # part of a package, set py_modules to a list of the names of the modules
    # (minus the .py extension) in order to make setuptools aware of them:
    # py_modules=["mypackage"],
    install_requires=REQUIRED,
    python_requires=REQUIRES_PYTHON,
    include_package_data=True,
)

""" Old setup.py """
#! /usr/bin/env python3 -i

""" pip install -r requirements.txt """
# Note: To use the 'upload' functionality of this file, you must:
#   $ pipenv install twine --dev
import io
import sys


# Package meta-data.
NAME = "tgs-validation"
DESCRIPTION = ("Tools to help manipulate and validate tables containing data
                ultimately intended for METRC.")
URL = "https://github.com/jcarpinelli/tgs-validation"
EMAIL = "jcarpinelli@acm.org"
AUTHOR = "Joseph Carpinelli"
REQUIRES_PYTHON = ">=3.6.0"
VERSION = "0.0.1"
# Required Packages
REQUIRED = [
    openpyxl

# Optional Packages
EXTRAS = {
    # 'fancy feature': ['django'],

with open("LICENSE") as f:

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!
# If you do change the License, remember to change the Trove Classifier for that!
here = os.path.abspath(os.path.dirname(__file__))
# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
    with io.open(os.path.join(here, "README.md"), encoding="utf-8") as f:
except FileNotFoundError:

about = {}
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(here, project_slug, "__version__.py")) as f:
else:

class UploadCommand(Command):
    """Support setup.py upload."""
    description = "Build and publish the package."
    user_options = []
    @staticmethod
    def status(s):
        """Prints things in bold."""
        print("\033[1m{0}\033[0m".format(s))
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        try:
            self.status("Removing previous builds…")
            rmtree(os.path.join(here, "dist"))
        except OSError:
            pass
        self.status("Building Source and Wheel (universal) distribution…")
        os.system("{0} setup.py sdist bdist_wheel --universal".format(sys.executable))
        self.status("Uploading the package to PyPI via Twine…")
        os.system("twine upload dist/*")
        self.status("Pushing git tags…")
        os.system("git tag v{0}".format(about["__version__"]))
        os.system("git push --tags")
        sys.exit()

# Where the magic happens:
    name=NAME,
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    author_email=EMAIL,
    url=URL,
    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"]),
    # If your package is a single module, use this instead of "packages":

    # entry_points={
    #     "console_scripts": ["mycli=mymodule:cli"],
    # },
    extras_require=EXTRAS,
    license="MIT",
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy"
    ],
    # $ setup.py publish support.
    cmdclass={
        "upload": UploadCommand,
    },

