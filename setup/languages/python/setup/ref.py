################################ Build ################################
"""
Although not required, it’s common to locally install your project in
“editable” or “develop” mode while you’re working on it. This allows your
project to be both installed and editable in project form.

Assuming you’re in the root of your project directory, then run:

pip install -e .


Source distributions

Minimally, you should create a Source Distribution:

python setup.py sdist

A “source distribution” is unbuilt (i.e. it’s not a Built Distribution),
and requires a build step when installed by pip. Even if the distribution
is pure Python (i.e. contains no extensions), it still involves a build step
to build out the installation metadata from setup.py.

Wheels

You should also create a wheel for your project. A wheel is a built package
that can be installed without needing to go through the “build” process.
Installing wheels is substantially faster for the end user than installing
from a source distribution.

If your project is pure Python (i.e. contains no compiled extensions) and
natively supports both Python 2 and 3, then you’ll be creating what’s called
a *Universal Wheel* (see section below).

If your project is pure Python but does not natively support both Python 2 and
3, then you’ll be creating a “Pure Python Wheel” (see section below).

If your project contains compiled extensions, then you’ll be creating what’s
called a *Platform Wheel* (see section below).

Before you can build wheels for your project, you’ll need to install the wheel
package:

pip install wheel

Universal Wheels

Universal Wheels are wheels that are pure Python (i.e. contain no compiled
extensions) and support Python 2 and 3. This is a wheel that can be installed
anywhere by pip.

To build the wheel:

python setup.py bdist_wheel --universal

You can also permanently set the --universal flag in setup.cfg (e.g., see
sampleproject/setup.cfg):

[bdist_wheel]
universal=1

Only use the --universal setting, if:

    Your project runs on Python 2 and 3 with no changes (i.e. it does not
    require 2to3).
    Your project does not have any C extensions.

Beware that bdist_wheel does not currently have any checks to warn if you use
the setting inappropriately.

If your project has optional C extensions, it is recommended not to publish a
universal wheel, because pip will prefer the wheel over a source installation,
and prevent the possibility of building the extension.

Pure Python Wheels

Pure Python Wheels that are not “universal” are wheels that are pure Python
(i.e. contain no compiled extensions), but don’t natively support both Python
2 and 3.

To build the wheel:

python setup.py bdist_wheel -d TARGET

bdist_wheel will detect that the code is pure Python, and build a wheel that’s
named such that it’s usable on any Python installation with the same major
version (Python 2 or Python 3) as the version you used to build the wheel. For
details on the naming of wheel files, see PEP 425.

If your code supports both Python 2 and 3, but with different code (e.g., you
use “2to3”) you can run setup.py bdist_wheel twice, once with Python 2 and
once with Python 3. This will produce wheels for each version.

Platform Wheels

Platform Wheels are wheels that are specific to a certain platform like Linux,
macOS, or Windows, usually due to containing compiled extensions.

To build the wheel:

python setup.py bdist_wheel

bdist_wheel will detect that the code is not pure Python, and build a wheel
that’s named such that it’s only usable on the platform that it was built on.
For details on the naming of wheel files, see PEP 425.

Note

PyPI currently supports uploads of platform wheels for Windows, macOS, and the
multi-distro manylinux1 ABI. Details of the latter are defined in PEP 513.

Uploading your Project to PyPI

When you ran the command to create your distribution, a new directory dist/
was created under your project’s root directory. That’s where you’ll find your
distribution file(s) to upload.

Note

These files are only created when you run the command to create your
distribution. This means that any time you change the source of your project
or the configuration in your setup.py file, you will need to rebuild these
files again before you can distribute the changes to PyPI.

Note

Before releasing on main PyPI repo, you might prefer training with the PyPI
test site which is cleaned on a semi regular basis. See Using TestPyPI on how
to setup your configuration in order to use it.

Warning

In other resources you may encounter references to using python setup.py
register and python setup.py upload. These methods of registering and
uploading a package are strongly discouraged as it may use a plaintext HTTP or
unverified HTTPS connection on some Python versions, allowing your username
and password to be intercepted during transmission.

Tip

The reStructuredText parser used on PyPI is not Sphinx! Furthermore, to ensure
safety of all users, certain kinds of URLs and directives are forbidden or
stripped out (e.g., the .. raw:: directive). Before trying to upload your
distribution, you should check to see if your brief / long descriptions
provided in setup.py are valid. You can do this by following the instructions
for the pypa/readme_renderer tool.
Create an account

First, you need a PyPI user account. You can create an account using the form
on the PyPI website.

Note

If you want to avoid entering your username and password when uploading, you
can create a $HOME/.pypirc file with your username and password:

[pypi]
username = <username>
password = <password>

Be aware that this stores your password in plaintext.
Upload your distributions

Once you have an account you can upload your distributions to PyPI using twine.

The process for uploading a release is the same regardless of whether or not
the project already exists on PyPI - if it doesn’t exist yet, it will be
automatically created when the first release is uploaded.

For the second and subsequent releases, PyPI only requires that the version
number of the new release differ from any previous releases.

twine upload dist/*

You can see if your package has successfully uploaded by navigating to the
URL https://pypi.org/project/<sampleproject> where sampleproject is the name
of your project that you uploaded. It may take a minute or two for your
project to appear on the site.
"""

################################ Reference ################################
# For 'upload' functionality
# $ pipenv install twine --dev
# 
# The essence of semantic versioning is a 3-part MAJOR.MINOR.MAINTENANCE
# numbering scheme, where the project author increments:
#   MAJOR version when they make incompatible API changes,
#   MINOR version when they add functionality in a backwards-compatible manner,
#   MAINTENANCE version when they make backwards-compatible bug fixes.
# 
# 1.2.0.dev1  # Development release
# 1.2.0a1     # Alpha Release
# 1.2.0b1     # Beta Release
# 1.2.0rc1    # Release Candidate
# 1.2.0       # Final Release
# 1.2.0.post1 # Post Release
# 15.10       # Date based release
# 23          # Serial release
#
# Optional Packages
# EXTRAS={
#     
# }
# extras_require=EXTRAS
#
# Including Data Files
# PACKAGE_DATA={'sample': ["package_data.dat"],}
# 
# Although configuring package_data is sufficient for most needs, in some
# cases you may need to place data files outside of your packages. The
# data_files directive allows you to do that. It is mostly useful if you need
# to install files which are used by other programs, which may be unaware of
# Python packages.
# Each (directory, files) pair in the sequence specifies the installation
# directory and the files to install there. The directory must be a relative
# path (although this may change in the future, see wheel Issue #92). and it
# is interpreted relative to the installation prefix (Python’s sys.prefix for
# a default installation; site.USER_BASE for a user installation). Each file
# name in files is interpreted relative to the setup.py script at the top of
# the project source distribution.
# DATA_FILES=[("my_data", ["data/data_file"])]
#
# Project URL Examples
#     'Documentation': 'https://packaging.python.org/tutorials/distributing-packages/',
#     'Funding': 'https://donate.pypi.org',
#     'Say Thanks!': 'http://saythanks.io/to/example',
#     'Source': 'https://github.com/pypa/sampleproject/',
#     'Tracker': 'https://github.com/pypa/sampleproject/issues',
# 
# entry_points={
#     "console_scripts": ["mycli=mymodule:cli"],
# },
# $ setup.py publish support.
# cmdclass={
#    "upload": UploadCommand,
# }

# ref.py
DIRECTORIES = {
    "HTML": [".html5", ".html", ".htm", ".xhtml"],
    "IMAGES": [".jpeg", ".jpg", ".tiff", ".gif", ".bmp", ".png", ".bpg", "svg",
               ".heif", ".psd"],
    "VIDEOS": [".avi", ".flv", ".wmv", ".mov", ".mp4", ".webm", ".vob", ".mng",
               ".qt", ".mpg", ".mpeg", ".3gp"],
    "DOCUMENTS": [".oxps", ".epub", ".pages", ".docx", ".doc", ".fdf", ".ods",
                  ".odt", ".pwi", ".xsn", ".xps", ".dotx", ".docm", ".dox",
                  ".rvg", ".rtf", ".rtfd", ".wpd", ".xls", ".xlsx", ".ppt",
                  "pptx"],
    "ARCHIVES": [".a", ".ar", ".cpio", ".iso", ".tar", ".gz", ".rz", ".7z",
                 ".dmg", ".rar", ".xar", ".zip"],
    "AUDIO": [".aac", ".aa", ".aac", ".dvf", ".m4a", ".m4b", ".m4p", ".mp3",
              ".msv", "ogg", "oga", ".raw", ".vox", ".wav", ".wma"],
    "PLAINTEXT": [".txt", ".in", ".out"],
    "PDF": [".pdf"],
    "PYTHON": [".py"],
    "XML": [".xml"],
    "EXE": [".exe"],
    "SHELL": [".sh"]

}


FILE_FORMATS = {file_format: directory
                for directory, file_formats in DIRECTORIES.items()
                for file_format in file_formats}

def organize_junk():
    for entry in os.scandir():
        if entry.is_dir():
            continue
        file_path = Path(entry)
        file_format = file_path.suffix.lower()
        if file_format in FILE_FORMATS:
            directory_path = Path(FILE_FORMATS[file_format])
            directory_path.mkdir(exist_ok=True)
            file_path.rename(directory_path.joinpath(file_path))

        for dir in os.scandir():
            try:
                os.rmdir(dir)
            except:
                pass


import os 
from pathlib import Path 
  
DIRECTORIES = { 
    "HTML": [".html5", ".html", ".htm", ".xhtml"], 
    "IMAGES": [".jpeg", ".jpg", ".tiff", ".gif", ".bmp", ".png", ".bpg", "svg", 
               ".heif", ".psd"], 
    "VIDEOS": [".avi", ".flv", ".wmv", ".mov", ".mp4", ".webm", ".vob", ".mng", 
               ".qt", ".mpg", ".mpeg", ".3gp"], 
    "DOCUMENTS": [".oxps", ".epub", ".pages", ".docx", ".doc", ".fdf", ".ods", 
                  ".odt", ".pwi", ".xsn", ".xps", ".dotx", ".docm", ".dox", 
                  ".rvg", ".rtf", ".rtfd", ".wpd", ".xls", ".xlsx", ".ppt", 
                  "pptx"], 
    "ARCHIVES": [".a", ".ar", ".cpio", ".iso", ".tar", ".gz", ".rz", ".7z", 
                 ".dmg", ".rar", ".xar", ".zip"], 
    "AUDIO": [".aac", ".aa", ".aac", ".dvf", ".m4a", ".m4b", ".m4p", ".mp3", 
              ".msv", "ogg", "oga", ".raw", ".vox", ".wav", ".wma"], 
    "PLAINTEXT": [".txt", ".in", ".out"], 
    "PDF": [".pdf"], 
    "PYTHON": [".py"], 
    "XML": [".xml"], 
    "EXE": [".exe"], 
    "SHELL": [".sh"] 
  
} 
  
FILE_FORMATS = {file_format: directory 
                for directory, file_formats in DIRECTORIES.items() 
                for file_format in file_formats} 
  
def organize_junk(): 
    for entry in os.scandir(): 
        if entry.is_dir(): 
            continue
        file_path = Path(entry) 
        file_format = file_path.suffix.lower() 
        if file_format in FILE_FORMATS: 
            directory_path = Path(FILE_FORMATS[file_format]) 
            directory_path.mkdir(exist_ok=True) 
            file_path.rename(directory_path.joinpath(file_path)) 
  
        for dir in os.scandir(): 
            try: 
                os.rmdir(dir) 
            except: 
                pass
  
if __name__ == "__main__": 
    organize_junk() 







y.sort(key=os.path.splitext)

y=[f for f in os.listdir(dirname)
    if os.path.isfile(os.path.join(dirname, f))]

################################ Reference ################################

