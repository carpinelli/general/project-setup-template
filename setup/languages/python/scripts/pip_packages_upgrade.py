#!/usr/bin/env python3

"""Updates all Python packages installed with pip."""

import pkg_resources
from subprocess import call


def main() -> None:
    packages = [dist.project_name for dist in pkg_resources.working_set]
    call("pip install --upgrade " + " ".join(packages), shell=True)


if __name__ == "__main__":
    main()

