#!/bin/bash

# React/Redux project creation script.
# Joseph Carpinelli: 2024-01-06.

## =================================================================== ##

function createReactReduxProject()
{
  PROJECT_NAME="$1"
  PROJECT_DIR="$HOME/src/web/frontend/$PROJECT_NAME"
  mkdir -p "$PROJECT_DIR"
  cd "$PROJECT_DIR"

  createMernClient $@
  gitSetup $@
  printFiles $@

  return 0
}


## =================================================================== ##

function main()
{
  createReactReduxProject $@
  return 0
}


main $@
exit 0


## =================================================================== ##

