#!/bin/bash

# MERN frontend project setup functions.
# Joseph Carpinelli: 2024-01-06.

## =================================================================== ##

# Bash.

function console()
{
    printf '%s\n' "$@"
    return 0
}


## =================================================================== ##


# Files.

function copyClientFiles()
{
  cp -av $HOME/src/general/project-setup-template/languages/ecma/files/client/. ./
  return 0
}

function printFrontendFiles()
{
  console "frontend/"
  console "1. LICENSE"
  console "2. README.md"
  console "3. CONTRIBUTING.md"
  console "4. scripts/"
  console "5. tsconfig.ts"
  console "6. docs/"
  console "\t 1. wireframes"
  console "\t 2. kanban/stories"
  console "7. src/"
  console "\t\t 1. components/"
  console "\t\t 2. features/"
  console "\t\t 3. pages/"
  console "\t\t 4. main.{js,ts}"
  console "\t\t 5. App.{jsx,tsx}"
  console ""
  console "Edit: "
  console "1. README.md"
  console "\t 1. Add Project Name and Description."
  console "2. main.{js,ts}"
  console "3. App.{jsx,tsx}"

  return 0
}


## ===================================================================== ##


# Client/Frontend.

function installReactRedux()
{
  # React.
  npm install react react-dom react-router-dom
  npm install -D @types/react @types/react-dom @types/react-router-dom
  # Redux.
  npm install @reduxjs/toolkit react-redux
  # Axios.
  npm install axios
  # React + Markdown.
  npm install react-markdown

  return 0
}

function installTailwindCSS()
{
  # Install Tailwind CSS.
  npm install -D tailwindcss postcss autoprefixer
  npx tailwindcss init -p

  return 0
}


## ==================================================================== ##

# Main

function createMernClient()
{
  # Create React project with Vite.
  npm create vite@latest "./" -- --template react-ts
  npm install
  installTypeScript $@
  installReactRedux $@
  installTailwindCSS $@
  # The next two were commented out in the MERN script.
  copyBasicProjectFiles $@
  copyClientFiles $@

  return 0
}


## ==================================================================== ##

