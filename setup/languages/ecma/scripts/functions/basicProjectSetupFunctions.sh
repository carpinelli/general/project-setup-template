#!/bin/bash

# Basic project setup functions.
# Joseph Carpinelli: 2024-01-06.

## =================================================================== ##

# Bash.

function console()
{
    printf '%s\n' "$@"
    return 0
}


## =================================================================== ##

# Files.

function copyBasicProjectFiles()
{
  cp $HOME/src/general/project-setup-template/LICENSE ./
  cp $HOME/src/general/project-setup-template/README.md ./
  cp $HOME/src/general/project-setup-template/CONTRIBUTING.md ./

  return 0
}


## =================================================================== ##

# Git.

function gitSetup()
{
    # Removes project template's git folder
    console "Run: "
    console "rm -rf .git/"
    # Setup Git for a new project
    console "git init --initial-branch=main"
    #groupPath="GET_GROUP_PATH_OR_WHOLE_URL_FROM_USER"
    #repo_name=$(git rev-parse --show-toplevel | xargs basename).git
    #branch_name=$(git rev-parse --abbrev-ref HEAD)
    #console "Run: "
    #console "$ git remote add origin git@gitlab.com:<namespace/username>/$groupPath/$repo_name $branch_name"
    # Self-hosted:
    #git remote add origin git@gitlab.example.com:<namespace/username>/$groupPath/$repo_name $branch_name
    console "git add ."
    console "git commit -m "Initial commit.""
    console "$ git remote add origin git@gitlab.com:<namespace/username>/$groupPath/$repo_name $branch_name"
    console "$ git push --set-upstream origin main"
    
    return 0
}


## =================================================================== ##

# Main

function basicProjectSetup()
{
  copyBasicProjectFiles $@
  gitSetup $@

  return 0
}


## =================================================================== ##


# basicProjectSetup $@
# gitSetup $@
# exit 0


## =================================================================== ##

