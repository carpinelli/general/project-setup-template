#!/bin/bash

# Install TypeScript function.
# Joseph Carpinelli: 2024-01-06.

## ===================================================================== ##

# Main

function installTypeScript()
{
  npm install -D typescript
  # Copy tsconfig.json instead.
  # tsc --init
  
  return 0
}


## ===================================================================== ##

