#!/bin/bash

# MERN stack project creation script.
# Joseph Carpinelli: 2024-01-06.

## ===================================================================== ##

function createMernStackProject()
{
  PROJECT_NAME="$1"
  # FOO will be assigned 'default' value if VARIABLE not set or null.
  # The value of VARIABLE remains untouched.
  #FOO="${VARIABLE:-default}"
  # If VARIABLE not set or null, set it's value to 'default'.
  # Then that value will be assigned to FOO.
  #FOO="${VARIABLE:=default}"
  PROJECT_BASE="${2:=${HOME}/src/web/mern/}"
  PROJECT_DIR="$PROJECT_BASE/$PROJECT_NAME"
  mkdir -p "$PROJECT_DIR"
  cd "$PROJECT_DIR"

  source ./functions/basicProjectSetupFunctions.sh
  source ./functions/frontendSetupFunctions.sh
  source ./functions/backendSetupFunctions.sh
  createMernServer $@
  cd ..
  cd client
  createMernClient $@
  cd ..
  copyMernFiles $@
  basicProjectSetup $@
  printFiles $@

  return 0
}


## ===================================================================== ##


function main()
{
  createMernStackProject $@
  return 0
}


main $@
exit 0

## ===================================================================== ##

