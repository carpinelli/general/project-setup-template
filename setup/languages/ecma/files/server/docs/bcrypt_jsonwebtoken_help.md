# Author: Dom.


# Bcrypt
npm i bcrypt
// encryption package for passwords
// require at top const bcrypt = require('bcrypt')
// Ex. const encryptedPassword = await bcrpyt.hash(<plain text pw>, bcrypt.genSalt(<number>))
// <number> for length, larger takes longer but more secure, example 10,
// if hidden in .env make sure to wrap in Number() to convert from string.
// Ex.   validating and comparing hashed password from DB to plaintext from User
// const validPass = await bcrypt.compare(req.body.password, foundUser.password)


# JWT / jsonwebtoken
npm i jsonwebtoken
// https://www.npmjs.com/package/jsonwebtoken
// used to generate tokens for users after they verify their user identity
// require at the top const jwt = require('jsonwebtoken')
// in .env store -> JWT_SECRET = banana (can also store SALT_ROUNDS for use in bcrypt above)
// Ex. const paylod = { username: localStoredUserName}
//     const token = jwt.sign(payload, <secret>, { expiresIn: 300 })
//     res.status(200).json({ token }) send off the token
// jwt.verify(token, secret)
